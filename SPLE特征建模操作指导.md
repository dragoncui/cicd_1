## SPLE特征建模

### 步骤
	特征建模层级关系：领域->子系统->部件->特性；
	因存在层级关系因此在进行部分操作时需注意下层级是否存在内容，如：删除、添加特性
#### 1、添加领域
- 点击左侧菜单栏的添加按钮，在展开的内容中选择添加领域，弹出框填写领域中英文名称(***必填***)，添加完左侧菜单栏可见新增领域

[![](http://image.huawei.com/tiny-lts/v1/images/916574dd4b4e87bfce1c7a24278ec107_2387x1149.png)](http://image.huawei.com/tiny-lts/v1/images/916574dd4b4e87bfce1c7a24278ec107_2387x1149.png)

[![](http://image.huawei.com/tiny-lts/v1/images/00dd4377df43b541f03a4cb85031e9c0_2332x1145.png)](http://image.huawei.com/tiny-lts/v1/images/00dd4377df43b541f03a4cb85031e9c0_2332x1145.png)

#### 2、添加子系统
- 点击左侧菜单栏的添加按钮，在展开的内容中选择添加子系统，弹出框选择将所添加的子系统的所属领域(***必填***)、编号(如：01，长度不小于2且编号唯一不可重复)(***必填***)、中英文名称(***必填***)以及其余选填内容，添加完成后在其所属领域下可见新增子系统

[![](http://image.huawei.com/tiny-lts/v1/images/2ed7bea5e3efb28d3ea9e70963eb6e20_2335x1165.png)](http://image.huawei.com/tiny-lts/v1/images/2ed7bea5e3efb28d3ea9e70963eb6e20_2335x1165.png)

#### 3、添加部件
- 点击左侧菜单栏的添加按钮，在展开的内容中选择添加部件，弹出框选择将所添加的部件的所属子系统(***必填***)、编号(选择所属子系统后生成：**所属子系统编号-1** 依次递增的编号，或输入以：**所属子系统编号-XX** 格式的编号，XX号段只能包含数字、大小写字母和下划线)(***必填***)、中英文名称(***必填***)以及其余选填内容，添加完成后在其所属子系统可见新增部件

[![](http://image.huawei.com/tiny-lts/v1/images/990d2b48983c9b3d68cfc8b47ff7d93b_1734x836.png)](http://image.huawei.com/tiny-lts/v1/images/990d2b48983c9b3d68cfc8b47ff7d93b_1734x836.png)

#### 4、领域、子系统及部件的其他操作
##### 1. 属性修改
- 领域属性修改

[![](http://image.huawei.com/tiny-lts/v1/images/4975ff71eb8feff9ef1cd761f0946036_1740x967.png)](http://image.huawei.com/tiny-lts/v1/images/4975ff71eb8feff9ef1cd761f0946036_1740x967.png)

[![](http://image.huawei.com/tiny-lts/v1/images/92214260ddd9d8bd6a86a1aa7764901e_1740x962.png)](http://image.huawei.com/tiny-lts/v1/images/92214260ddd9d8bd6a86a1aa7764901e_1740x962.png)

- 子系统属性修改及人员管理

[![](http://image.huawei.com/tiny-lts/v1/images/7cbe80f8553e8771d2057866633dde3a_1740x967.png)](http://image.huawei.com/tiny-lts/v1/images/7cbe80f8553e8771d2057866633dde3a_1740x967.png)

[![](http://image.huawei.com/tiny-lts/v1/images/5355756f915b96c9b864136831c75952_1740x967.png)](http://image.huawei.com/tiny-lts/v1/images/5355756f915b96c9b864136831c75952_1740x967.png)

- 成员添加(按姓名或工号)、查找，成员职务、所属部件、角色更改

[![](http://image.huawei.com/tiny-lts/v1/images/f64467f916cbbac09522bccb7e365c3f_1740x967.png)](http://image.huawei.com/tiny-lts/v1/images/f64467f916cbbac09522bccb7e365c3f_1740x967.png)

- 部件属性修改

[![](http://image.huawei.com/tiny-lts/v1/images/c050668f9d3412bde44c9a21a35251f8_2025x1113.png)](http://image.huawei.com/tiny-lts/v1/images/c050668f9d3412bde44c9a21a35251f8_2025x1113.png)

- 部件可配置特性编辑

[![](http://image.huawei.com/tiny-lts/v1/images/12752374d17ba57ee3cb16e2d8272987_1703x1081.png)](http://image.huawei.com/tiny-lts/v1/images/12752374d17ba57ee3cb16e2d8272987_1703x1081.png)

选择型：

	布尔(ture/false，选项除编码外可编辑)
	单选/多选(选项内容自定义)

[![](http://image.huawei.com/tiny-lts/v1/images/d5fa640f804957f0ff27008dbeb36053_1696x1074.png)](http://image.huawei.com/tiny-lts/v1/images/d5fa640f804957f0ff27008dbeb36053_1696x1074.png)

录入型：

	文本(默认值、单位)
	整数(最小值、最大值、默认值、单位)
	小数(最小值、最大值、小数位数、默认值、单位)

[![](http://image.huawei.com/tiny-lts/v1/images/c666db418e56ec77626f47b1480d3b59_1708x1083.png)](http://image.huawei.com/tiny-lts/v1/images/c666db418e56ec77626f47b1480d3b59_1708x1083.png)

##### 2. 删除
- 删除领域(子系统、部件同操作)

[![](http://image.huawei.com/tiny-lts/v1/images/81014837ba505e2588e6534b83cff03c_2010x1108.png)](http://image.huawei.com/tiny-lts/v1/images/81014837ba505e2588e6534b83cff03c_2010x1108.png)

[![](http://image.huawei.com/tiny-lts/v1/images/57ad29343d42304920ecfb5d6f0352f6_2004x1020.png)](http://image.huawei.com/tiny-lts/v1/images/57ad29343d42304920ecfb5d6f0352f6_2004x1020.png)

##### 3. 子系统、部件清单导入/导出
- 点击左侧菜单栏右上角按钮，点击子系统清单(部件清单)进入清单界面，该界面同样可进行添加、编辑修改、删除的操作

[![](http://image.huawei.com/tiny-lts/v1/images/07bd85e639dd2f1ee97cd15d2af73e99_2010x1047.png)](http://image.huawei.com/tiny-lts/v1/images/07bd85e639dd2f1ee97cd15d2af73e99_2010x1047.png)

- 点击导入按钮，在所弹出弹窗点击下载相关模板(文件类型：***xlsx***)，按模板内容将相关信息填入后点击导入并上传文件(一次限制导入***5000***条数据，最大***10M***)

[![](http://image.huawei.com/tiny-lts/v1/images/318a608da326b4cdadb583104c34e196_2028x1055.png)](http://image.huawei.com/tiny-lts/v1/images/318a608da326b4cdadb583104c34e196_2028x1055.png)

- 点击左侧菜单栏右上角按钮，点击导入，在所弹出弹窗也可下载相关模板或按模板对数据进行导入

[![](http://image.huawei.com/tiny-lts/v1/images/c0ff89ce4df6464c74f68ecf45136cfc_2021x1117.png)](http://image.huawei.com/tiny-lts/v1/images/c0ff89ce4df6464c74f68ecf45136cfc_2021x1117.png)

[![](http://image.huawei.com/tiny-lts/v1/images/5f3edc61e261f0745ad0a8a553caedcc_2022x1121.png)](http://image.huawei.com/tiny-lts/v1/images/5f3edc61e261f0745ad0a8a553caedcc_2022x1121.png)

- 子系统、部件清单页面点击导出按钮后将直接下载相应清单文件(文件类型：xlsx)至本地

[![](http://image.huawei.com/tiny-lts/v1/images/a2bcd4a8eb0bab801a5ed53f4abe9951_2020x1124.png)](http://image.huawei.com/tiny-lts/v1/images/a2bcd4a8eb0bab801a5ed53f4abe9951_2020x1124.png)

#### 5、添加特性
- 点击菜单栏已建部件进入该路径：
>首页>特征建模>领域>子系统>部件>特性

[![](http://image.huawei.com/tiny-lts/v1/images/a82fad312d684b1a34851a4f760d3e17_2014x1123.png)](http://image.huawei.com/tiny-lts/v1/images/a82fad312d684b1a34851a4f760d3e17_2014x1123.png)

- 点击添加按钮，弹出框填写中文名称(必填)、编号(以：所属子系统编号-XX 格式的编号，XX号段只能包含数字)(必填)、以及其余选填内容，添加完成后在其所属子系统可见新增部件

[![](http://image.huawei.com/tiny-lts/v1/images/89035e098065f9beb0a1fd346c4b5a4a_2215x1116.png)](http://image.huawei.com/tiny-lts/v1/images/89035e098065f9beb0a1fd346c4b5a4a_2215x1116.png)